##### README

In this exercise, we use the material we have from [Session 4](../session04) that creates instantiates a web server and enables SSL.

Our exercice session05 is to deploy the following `index.php` file in `/var/www/html/` on all web servers specifying that the website should use the database 

``` php
<?php

$db_host = ; // Insert the host name or IP address from ansible hostvars
$db_name = ; // Insert the DB name from ansible variable
$db_username = ; // Insert DB username from ansible variable
$db_password = ; // Insert DB password from ansible varible

```

Additionally on the database server, you will want to open the firewall such that web1 and web2 can connect on TCP port 3306. While we did not yet configure MariaDB on the db1 server, you might consider using the [MariaDB](https://galaxy.ansible.com/bertvv/mariadb) role to do so safely
